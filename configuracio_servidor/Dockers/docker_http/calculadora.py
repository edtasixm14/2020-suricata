#!/usr/bin/python

'''Test CGI script.
'''

import sys
import cgi

# Debug
#import cgitb; cgitb.enable()

write = sys.stdout.write

form = cgi.FieldStorage()

# headers
write("Content-Type: text/html; charset=UTF-8\r\n") # or "text/html"...
write("\r\n")

operacio=form.getvalue('operacio')
primeroperand=form.getvalue('primeroperand')
segonoperand=form.getvalue('segonoperand')

# Si tots dos operands estan buits:
if not isinstance(primeroperand,str) and not isinstance(segonoperand,str):
    resultat = "NULL. Els operands no poden estar buits"

# Evaluem els casos en que algun dels operands siguin buits:
elif not isinstance(primeroperand,str):
    resultat = float(segonoperand)

elif not isinstance(segonoperand,str):
    resultat = float(primeroperand)

# Si tots dos operands existeixen
else:
    # Passem els valors a float
    primeroperand=float(primeroperand)
    segonoperand=float(segonoperand)

    # Realitzem les operacions
    if operacio == 'suma':
        resultat = primeroperand + segonoperand
    elif operacio == 'resta':
        resultat = primeroperand - segonoperand
    elif operacio == 'multiplicacio':
        resultat = primeroperand * segonoperand
    else:
        resultat = primeroperand / segonoperand

print "El resultat es:", resultat

sys.exit(0)

# vim:sw=4:ts=4:ai:et
