#/bin/bash

# Copiem els fitxers de configuració
cp /opt/docker/vsftpd.conf /etc/vsftpd/vsftpd.conf
rm -rf /var/ftp/pub

# Copiem els fitxers del servidor
cp HowTo-ASIX-Docker-Cloud-Machine-Swarm.pdf /var/ftp/
cp HowTo-ASIX-Kerberos.pdf /var/ftp/
cp HowTo-ASIX-LVM.pdf /var/ftp/
cp HowTo-ASIX-RAID.pdf /var/ftp/
cp HowTo-ASIX-Tunnel_SSH.pdf /var/ftp/
cp HowTo-ASIX-VPN.pdf /var/ftp/
