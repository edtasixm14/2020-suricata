# Directori Servidor

Aquest directori, esta dividit en diverses parts:

- Dockers, on es podran trobar com hem creat alguns dels Dockers utilitzats per a la creació del nostre servidor
- Fitxers, on es trobaran tots els fitxers necessaris per a la correcta configuració del projecte que estem muntant
- servidor.md, un Howto on s'explica pas per pas com s'ha de fer per a tenir correctament configurat el Suricata
