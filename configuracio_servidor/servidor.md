# Servidor

Per començar amb la creació dels nostres servidors, el que farem serà crear
una maquina a Amazon la qual la utilitzarem com a servidor on es crearan tots els
dockers i on es configuraran alguns serveis per a que donin accés al exterior.

A més utilitzarem dos maquines més d'Amazon, les qual les utilitzarem per a fer
proves com a hosts locals de la xarxa del servidor.

El primer pas que haurem de fer serà la creació de les maquines i la configuració
dels seus ports, en el nostre cas deixarem de moment obert tots els ports.


![imagen](../aux/ports.png)
En aquesta imatge es pot observa les regles que hem assignat.

Un cop configurades les regles que utilitzarem a les maquines d'Amazon, ens
connectem a elles.

```
ssh -i mykey3.pem fedora@18.130.45.137
```

On mykey3.pem serà una clau que haurem configurat durant la creació de la màquina i
la IP dependrà en cada màquina.

Després d'iniciar sessió, des de el host que fem servir com a servidor llançarem la
ordre

```
docker-compose up -d
```

Que iniciarà els dockers que hi han dins el fitxer [docker-compose.yml](fitxers/docker-compose.yml).

El següent que configurarem serà un servidor http des de el nostre host local, el
primer pas, serà anar a la carpeta */etc/httpd/conf* on modificarem el fitxer [httpd.conf](fitxers/httpd.conf),
per a canviar el ServerNAme, el següent pas, serà anar a la carpeta */var/www/html/*,
on configurem el fitxer [index.html](fitxers/index.html).

Després de tenir correctament configurats tots els servidors, comencem amb la
instal·lació del Suricata

```
dnf install -y suricata
```

Un cop instal·lat el software, anirem al fitxer [suricata.yaml](fitxers/docker-compose.yml)
que es troba en la carpeta */etc/suricata*, dins d'aquest fitxer el primer que farem
serà configurar quines voldrem que siguin considerades les nostres ips privades
per a després poder assignar de manera més rapida les regles

El següent pas, serà accedir a la carpeta */etc/suricata/rules* on crearem els fitxers
al nostre gust per a poder començar amb la configuració de les regles

En el nostre cas, crearem tres fitxers [local.rules](fitxers/local.rules) el qual,
contindrà les regles que aniran dirigides al hosts locals, [extern.rules](fitxers/extern.rules)
per a poder controlar les connexions provinents de l'exterior i [custom.rules](fitxers/custom.rules),
el qual serà el nostre fitxer de proves

En el nostre cas, com voldrem fer servir Suricata com a IDS y IPS haurem de fer uns
passos extres, el primer serà comprovar que el nostre host té habilitat el NFQ, si és així, borrarem totes les regles que hi hagin assignades en les nostres iptables,
per assegurar que no hi ha problemes.

```
[fedora@ip-172-31-18-5 ~]$ suricata --build-info | grep NFQ
Features: NFQ PCAP_SET_BUFF LIBPCAP_VERSION_MAJOR=1 AF_PACKET HAVE_PACKET_FANOUT LIBCAP_NG LIBNET1.1 HAVE_HTP_URI_NORMALIZE_HOOK PCRE_JIT HAVE_NSS HAVE_LIBJANSSON TLS
  NFQueue support:                         yes
```

```
sudo iptables -F
```

Al haver realitzat els passos anteriors, el següent serà posar en marxa el suricata

```
sudo suricata -c /etc/suricata/suricata.yaml -q 0
```

Per a comprovar que tot ha arrancat correctament, podrem anar a donar un cop
d'ull a la carpeta */var/log/suricata/* on trobarem el fitxer [suricata.log](fitxers/suricata.log),
el qual, ens mostra els logs.

Al estar segurs que ha arrancat el nostre Suricata, haurem de fer uns canvis en les
iptables per a que Suricata funcioni com a IPS, ja que haurem de redirigir totes les
connexions que vulguem controlar per el Suricata, ho farem amb la següent ordre.

```
sudo iptables -I FORWARD -j NFQUEUE
sudo iptables -I INPUT -j NFQUEUE
sudo iptables -I OUTPUT -j NFQUEUE
```

Nosaltres utilitzem tres ja que volem tenir controlat tant els redireccionaments que
faci el nostre host, com tot el que entra o surt d'aquest.

L'últim pas que ens quedarà, serà la comprovació de que les regles establertes
funcionen correctament, de les quals podrem observar els logs a la carpeta
*/var/log/suricata* en els fitxers [fast.log](fitxers/fast.log) o si es prefereix
observar en format json a [eve.json](fitxers/eve.json).

Hem de tenir en compte que al tenir aquesta configuració, si tanquem el Suricata,
ens quedarem sense Internet, per a solucionar aquest problema només haurem de eliminar
les regles de les iptables com hem fet anteriorment.

En el cas de voler reiniciar el servei perquè hem canviat alguna regla s'ha de fer
amb la següent ordre

```
/bin/pkill -USR2 -f suricata
```

En el següent [fitxer](fitxers/cas_practic.md) es poden veure les proves realitzades
i els logs corresponents a aquestes
