# Tallafocs

En informàtica, un tallafocs (firewall) és la part d'un sistema informàtic o una
xarxa informàtica que està dissenyada per bloquejar l'accés no autoritzat,
permetent el mateix temps comunicacions autoritzades.

Es tracta d'un dispositiu o conjunt de dispositius configurats per permetre,
limitar, xifrar o desxifrar el trànsit entre els diferents àmbits sobre la
base d'un conjunt de normes i altres criteris.

Els tallafocs poden ser implementats en maquinari o programari, o en una
combinació de tots dos. Els tallafocs s'utilitzen amb freqüència per evitar
que els usuaris d'Internet no autoritzats tinguin accés a xarxes privades
connectades a Internet, especialment intranets. Tots els missatges que entrin o
surtin de la intranet passen a través del tallafocs, que examina cada missatge
i bloqueja aquells que no compleixen els criteris de seguretat especificats.

Un tallafocs correctament configurat afegeix una protecció necessària a la xarxa,
però que en cap cas s'ha de considerar suficient. La seguretat informàtica abasta
més àmbits i més nivells de treball i protecció contra virus, malware, entre
d'altres amenaces.


![imagen](../aux/firewall.png)

## Iptables

Les iptables són una utilitat de línia d'ordres per configurar el tallafocs. Pot
configurar-se directament amb iptables, o usant un dels molts frontend existents
de consola i gràfics. El terme iptables s'usa per IPv4, i el terme ip6tables per
IPv6. Tant iptables com ip6tables tenen la mateixa sintaxi, però algunes opcions
són específiques d'IPv4 o d'IPv6.

## Com l'utilitzarem

Principalment el Suricata serà un complement a aquest firewall, encara que la majoria
de les configuracions que hi hauran en el nostre projecte serà apartir del Suricata,
necessitarem configurar en algun moment les iptables del nostre host per a permetre
que el nostre software Suricata funcioni correctament.
