# AMAZON AWS

Amazon Web Services, és un conjunt d'eines i de serveis al cloud computing
d'Amazon. Entre les empreses que l'utilitzen es troben algunes com Reddit,
Foursquare, Pinterest, Netflix, la NASA o la CIA, i algunes espanyoles com
Mapfre, el FC Barcelona o Interflora.

## PERQUÈ S'UTILITZA?
Amazon AWS ofereix una gran quantitat d'eines i serveis per a pode crear un
entorn de treball al núvol, a més, els serveis d'Amazon estan preparats per a
tot tipus d'empreses, petites, mitjanes o grans companyies. Això és degut a
la possibilitat de poder escalar les instàncies o l'emmagatzemament segons la
necessitat de la pròpia empresa.

Algunes de les eines de les quals disposa Amazon són:

- Cloud Computing
- Bases de Dades
- Creació de xarxes virtuals
- Emmagatzemament i gestors de contingut
- Seguretat i Control D'accés

## PER A QUÈ L'UTILITZEM NOSALTRES?

Nosaltres l'utilitzarem per a generar diverses instàncies, una principal que
s'utilitzara com a servidor la qual tindrà Dockers amb serveis oberts i un parell
més, que seran utilitzades com a host locals d'aquell servidor per a poder
testejar millor la diferencia entre xarxes locals i externes del Suricata.

![imagen](../aux/aws.png)
