# REGLES
Suricata, funciona amb unes determinades regles que controlen el trànsit de la xarxa.
Aquestes regles les podem dividir en seccions per a entendre millor el funcionament,
les regles estan dividides per:

- Acció
- Protocol
- Xarxa
- Ports
- Direcció (Entre la xarxa local i la xarxa externa)
- Paràmetres

Un exemple simple de regles del suricata seria:

```
alert icmp $EXTERNAL_NET any -> $HOME_NET any (msg: "ICMP detectat provenint de l'exterior"; sid:2; rev:1;)
```

## Acció

```
**alert** icmp $EXTERNAL_NET any -> $HOME_NET any (msg: "ICMP detectat provenint de l'exterior"; sid:2; rev:1;)
```

Determina l'acció a realitzar quan una regla fa "match", és a dir, quan la regla
concorda amb la situació i s'executa el que la regla determina. Hi han 4 accions
diferents:

### Pass
Si la regla fa "match" i conté l'acció de pass, Suricata deixa d'escanejar els
paquets permeten-los doncs a la xarxa, també deixa de buscar més regles a aplicar.

### Drop
Només s'aplica en el mode IPS, si una regla fa "match" bloqueja el paquet, aquest
no s'envia al destí corresponent de la xarxa i posteriorment s'elimina. Quan això
passa, el destinatari no rep cap alerta de perquè no rep aquest paquet, però
Suricata si que genera alertes quan això succeeix, normalment és guarden al
fitxers de logs definit o bé per defecte o bé er l'administrador del sistema.

### Reject
És una manera activa de refusar un paquet. Tots dos, l'emissor i el receptor
rebran un paquet seleccionat automàticament. Si el paquet amb el que ha fet match
la regla és TCP s'envia un Reset-packet, per a qualsevol altre protocol s'envia
un paquet ICMP d'error. A part d'això, Suricata també genera una alerta al seu
fitxer de logs. Si Suricata està configurat com a IPS el paquet amb el que la
regla fa "match" s'elimina igual que amb la regla drop.

### Alert
Igual que el pass, el paquet es permetrà i no serà detectat com una amenaça, però,
la diferència entre el pass i l'Alert és que aquest últim genera una alerta al
fitxer de logs, que per tant, només la pot veure l'administrador del sistema, el
client de la xarxa que faci saltar aquesta regla no tindrà cap coneixement de l'alerta,
i el paquet el rebrà sense problema.

El mode IPS de Suricata pot bloquejar el trànsit de dues maneres, clarament és
amb les regles drop i reject.

Per defecte les regles s'apliquen en un determinat ordre, en cas de que n'hi
hagin algunes que puguin coincidir, per defecte l'ordre a aplicar és:

- pass
- drop
- reject
- alert

Aquest ordre d'aplicació el podem canviar al fitxer de configuració de Suricata
*/etc/suricata/suricata.yaml*.

## Protocol

```
alert **icmp** $EXTERNAL_NET any -> $HOME_NET any (msg: "ICMP detectat provenint de l'exterior"; sid:2; rev:1;)
```

El protocol s'indica amb una paraula clau per a que Suricata l'interpreti, hi han
quatre protocols bàsics:

1. tcp
2. udp
3. icmp
4. ip (també comprèn all o any per a indicar totes)

Suricata també interpreta altres protocols de la capa d'aplicació del model de
xarxa OSI, aquests protocols són:

- http
- ftp
- tls (També inclou ssl)
- smb
- dns
- dcerpc
- ssh
- smtp
- imap
- msn
- modbus *
- dnp3 *
- enip *
- nfs **
- ikev2 **
- krb5 **
- ntp **
- dhcp **

\* Per defecte estan desactivats  
\*\* Depèn de la xarxa, pot ser que estigui deprecated i no ho pugui detectar

## Xarxa

```
alert icmp **$EXTERNAL_NET** any -> **$HOME_NET** any (msg: "ICMP detectat provenint de l'exterior"; sid:2; rev:1;)
```

Pots assignar diferents rangs de direccions IP com a part d'una xarxa local, i
per tant, definir-les a les regles amb la variable $HOME_NET, d'aquesta manera
estalvies definir quines xarxes vols que siguin les locals i quines les externes,
les externes les pots utilitzar amb la variable $EXTERNAL_NET. Les direccions
de xarxa es poden configurar a */etc/suricata/suricata.yaml* d'aquesta manera:

```
HOME_NET: “[192.168.1.0/24,172.16.1.0/24]”

EXTERNAL_NET: any
```

També podem indicar les direccions amb els següents operadors per a agrupar-les
per rangs :

| Operador | Descripció |
| :------------- | :------------- |
| ../.. | Rangs d'IP (CIDR) |
| ! | Excepcions / Negacions |
| [.., ..] | Agrupa les direccions |

Exemples d'ús per a aclarir el funcionament:

| Exemple | Descripció |
| :------------- | :------------- |
| ! 8.8.8.8	 | Qualsevol IP **excepte** la 8.8.8.8 |
| $HOME_NET | La configuració del fitxer yaml conforme a la xarxa local |
| [$EXTERNAL_NET, !$HOME_NET] | Permet la xarxa externa i exclou la xarxa local |
| ![8.8.8.8, 10.10.10.10] | Qualsevol IP **excepte** la 8.8.8.8 i la 10.10.10.10 |
| [10.0.0.0/24, !10.0.0.1] | Permet la xarxa 10.0.0.0/24 però denega la direcció interna de la pròpia xarxa 10.0.0.1 |

## Ports

```
alert icmp $EXTERNAL_NET **any** -> $HOME_NET **any** (msg: "ICMP detectat provenint de l'exterior"; sid:2; rev:1;)
```

Podem indicar aquells ports on volem definir la regla, tant de la xarxa local com de la xarxa externa, igual que amb la xarxa, també podem jugar amb els operadors per a fer les regles més senzilles.

| Operador | Descripció |
| :------------- | :------------- |
| : | Rangs de ports |
| ! | Excepcions / Negacions |
| [.., ..] | Agrupa els ports |

Exemples d'ús per a aclarir el funcionament:

| Operador | Explicació |
| :------------- | :------------- |
| [80, 81, 82] | Port 80, 81, 82 |
| [80: 82] | Rang que comprèn el port 80, 81 i 82 |
| [1024: ] | Des del port 1024 fins al port més gran disponible |
| !80 | Tots els ports excepte el 80 |
| [80:100,!99] | Rang que comprèn des del port 80 al 100, excloent-hi el 99 |
| [1024: ] | Des del port 1024 fins al port més gran disponible |
| [1:80,![2,4]] | Rang del 1 al 80, excloent-hi els port 2 i 4 |

## Direcció

```
alert icmp $EXTERNAL_NET any **->** $HOME_NET any (msg: "ICMP detectat provenint de l'exterior"; sid:2; rev:1;)
```

Les regles poden actuar depenent de si el paquet es enviat des de la xarxa local
o des de la xarxa externa, això ho definim amb la direcció, i tenim dues opcions.
Pot anar només en una direcció, utilitzant la fletxa **->**, o bé que s'examinin
els paquets en totes dues direccions de la xarxa amb **<>**

## Paràmetres

```
alert icmp $EXTERNAL_NET any -> $HOME_NET any **(msg: "ICMP detectat provenint de l'exterior"; sid:2; rev:1;)**
```

Són opcionals, no afecten a l'inspecció de paquets de Suricata, només ho fan en
la manera en que aquest crea les alertes.

Hi han de diversos paràmetres, alguns dels que utilitzarem majorment són:

- msg:"Xarxa desconeguda";

Defineix un missatge textual per a l'alerta.

- sid:123;

Defineix un identificador propi per a la regla

- rev:123;

El SID gairebé sempre va acompanyat del rev, representa la versió de la regla.
Si una regla és modificada, le nombre del rev incrementarà automàticament.

- classtype: prova-suricata;

Podem crear classes i definir-les a les regles, aquestes classes tenen un ordre
d'execució, i podem definir quines volem que tinguin la prioritat.

La sintaxi per a crear una classe és: Classe, Alerta, Prioritat. Per exemple:

```
config classification: prova-suricata,Prova Suricata,1
```

D'aquesta manera podríem assignar a les regles la classe prova-suricata
