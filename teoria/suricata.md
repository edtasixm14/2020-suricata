# Suricata

Suricata és un software que conté tant la funció de IDS com IPS i a més, es
capaç de monitoritzar la xarxa, es una eina que pot treballar amb funcions multi fil. Això vol dir que una única instància, o sistema, pot distribuir la càrrega de feina entre tots els nuclis del processador disponibles, millorant d'aquesta manera el rendiment de l'aplicació sense necessitat d'obtenir algun hardware especial, gràcies a això l'eina pot processar un ample de banda màxim de 10 Gb per segon sense influir en el rendiment.

El fet de que Suricata sigui multi fil determina que una gran part d'administradors l'utilitzin per davant de *Snort*, que és un altre programa open source d'IDS/IPS molt utilitzat, però que no incorpora aquesta funció.

Suricata, destaca perquè a més de poder tenir controls sobre els protocols IP,
TCP, UDP e ICMP, també te claus per a poder treballar sobre FTP, HTTP, TLS, SMB.
Això ens permet poder escriure regles independentment del port que el protocol faci servir.

Les característiques més destacables són:

- Processament multi fil
- Captura de paquets
- Descodificador
- Detecció i comprovació amb les regles
- Processament de sortida de alertes
- Detecció automàtica dels protocols
- Anàlisis del rendiment

![imagen](../aux/suricata.jpg)

## Què és IPS i IDS.

### IPS

IPS (Intrusion prevention System), es un software que treballa en el control
d'accés a una xarxa informàtica per a protegir els nostres sistemes informàtics
de atacs, es un tipus de tecnologia molt semblant a la de un talla focs.


### IDS

Un IDS (Intrusion Detection System) es un sistema que tracta de detectar i alertar
sobre les intrusions que ens han intentat fer sobre un dels nostres sistemes o
xarxa, considerant com a intrusió tota activitat no autoritzada en un sistema, a
través de una monitorització de la xarxa y busqueda de patrons.

![imagen](../aux/ips_ids.png)

En la següent imatge es pot observar clarament la diferencia entre **IPS** i
**IDS**

![imagen](../aux/dif.jpg)
