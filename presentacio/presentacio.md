# Tallafocs

### Què és un tallafocs, o firewall?

### Com s'implanten dins d'una xarxa?

![](../aux/firewall.png)

# Iptables

### Què són les iptables?

![](../aux/iptables.jpeg)

# Iptables i Suricata

### Com podem implementar tots dos programes?

![](../aux/suricata_logo.jpg)

# IDS / IPS

## IPS

### Què volen dir les sigles IPS i IDS?

![](../aux/ips_ids.png)

# Diferencia entre **IPS** i **IDS**

![IDS observa mentre que IPS controla](../aux/dif_2.jpg)

# Suricata

Suricata és un software que pot actuar com un sistema IDS o bé com a IPS.

És multithread, a diferència de *Snort*, que és l'altre programa open source d'IDS/IPS més utilitzat.

La beta de Suricata va ser llançada al 2009, i la primera versió oficial va ser al juliol del 2010.

Actualment la versió més actual és la 5.0, amb cada nova versió s'hi acostumen a afegeixen millores de rendiment i noves regles per a poder controlar més protocols.

Alguns dels últims protocols afegits van ser *http* i *ssh*

![](../aux/suricata_logo.jpg)

# Suricata

Les caracteristiques més destacables són:

- Processament multifil
- Captura de paquets
- Procesament de sortida de alertes
- Detecció automàtica dels protocols
- Anàlisis del rendiment

![](../aux/suricata.jpg)

# Comandes de Suricata

### Per a què serveixen?

### Paràmetres

Alguns dels més rellevants són:

+ -h (Ajuda)
+ -V (Versió)
+ -c <ruta> (Fitxer de configuració)
+ -s <fitxer_regles> (Fitxer de regles per a afegir als actuals)
+ -S <fitxer_regles> (Únic fitxer de regles a executar)
+ -i <interficie> (Interfície de xarxa)

```
suricata -c /etc/suricata/suricata.yaml -i eth0 -S custom.rules
```

# Regles

### Què són les regles?

### Com estàn formades?

- Acció
- Protocol
- Xarxa
- Ports
- Direcció (Entre la xarxa local i la xarxa externa)
- Paràmetres

```
alert ssh $HOME_NET any -> $HOME_NET any (msg: "Connexió ssh local cap al servidor"; sid:101; rev:1;)
```

# Docker

### Què és Docker?

### Per a què els utilitzarem?

![](../aux/docker.png)

# Amazon

### Què és Amazon AWS i perquè s'utilitza?

Algunes de les eines de les quals disposa Amazon són:

- Cloud Computing
- Bases de Dades
- Creació de xarxes virtuals
- Emmagatzament i gestors de contingut
- Seguretat i Control d'accès

### Per a què l'utilitzem nosaltres?

![](../aux/aws.png)

# Instal·lació Suricata

![](../aux/suricata_esquema.png)

# Exemple pràctic Suricata

Realitzarem proves amb 3 instàncies d'Amazon que formaràn una xarxa local, i una altra instància que estarà a una xarxa externa.

### Controlant la xarxa

Fitxer que utilitzarem: **local.rules**

- SSH entre maquines "locals"
- Petició a http extern
- Alertar de pings entre xarxes locals
- Denegar el ping d'una maquina determinada a xarxes externes
- Host local es connecta al ftp local
- Host local no es connecta a http port !80
- Accés al servidor HTTP local entre les maquines locals
- Xarxes locals no poden utilitzar el daytimeserver del port 13

# Exemple pràctic Suricata

Realitzarem proves amb els nostres hosts locals de casa nostra, 3 instàncies d'Amazon que formaràn una xarxa local, i una altra instància que estarà a una xarxa externa.

### Casos amb la xarxa externa

Fitxer que utilitzarem: **extern.rules**

- Permetre ssh ips específiques i denegar els altres
- Avisar dels pings des de l'exterior
- Denegar accés http port 80
- Denegar accés ftp a qualsevol port
- Avisar accés servidor http port 8000
- Denegar ús del echo-server al port 7

![](../configuracio_servidor/fitxers/diagrama.png)
